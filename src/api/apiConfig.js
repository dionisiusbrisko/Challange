const apiConfig = {
    baseUrl:'https://api.themoviedb.org/3/',
    apiKey: '8bfe0983ef932377f51a6418e2e9f91b',
    originalImage:(imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    // w500image:(imgPath) => ''
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`
}